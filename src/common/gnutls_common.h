/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "common.h"
#include <gnutls/gnutls.h>

int encode_base64_and_send(gnutls_session_t *gnutls_session, void *buffer,
                           int buffer_size, int recipient_remote_socket_id);

int decode_base64_and_send(int sock, void *buffer, int buffer_size);

int read_data_from_gnutls_session(int sock, gnutls_session_t *gnutls_session,
                                  void **buffer, char *description,
                                  int *req_from_server_remote_socket_id,
                                  int close_on_error);
