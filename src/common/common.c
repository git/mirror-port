/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "common.h"

int splice_all(int from, int to, long long bytes) {
  // printf("calling splice_all %d -> %d \n", from, to);
  long result;
  result = splice(from, NULL, to, NULL, bytes, SPLICE_F_MOVE | SPLICE_F_MORE);
  if (result < 0)
    perror("splice");
  // printf("splice_all result -> %d \n", result);
  return result;
}

int transfer(int from, int to, long long bytes) {
  if (from == to) {
    printf("preventing self-loop socket %d -> %d \n", from, to);
    return 0;
  }

  int result;
  int pipes[2];

  result = pipe(pipes);
  if (result < 0)
    perror("transfer: pipe");

  // result from "from" because we need to know when there is no more data
  result = splice_all(from, pipes[1], bytes);
  if (result == 0)
    return -1; // no more data

  splice_all(pipes[0], to, bytes);

  close(pipes[1]);
  close(pipes[0]);

  return result;
}

int read_from_socket(int filedes, char *buffer, int *buffer_size) {
  int nbytes;
  nbytes = recv(filedes, buffer, MAXMSG - 1, 0);
  if (nbytes < 0) {
    perror("read");
    return -1;
  }
  *buffer_size = nbytes;
  return nbytes;
}

int read_from_socket2(int filedes, char **buffer, int *buffer_size) {
  if (fcntl(filedes, F_SETFL, fcntl(filedes, F_GETFL) | O_NONBLOCK) < 0) {
    // handle error
  }
  int nbytes;
  long int read_bytes = 0;
  int i = 0;
  do {
    if (i > 0) {
      *buffer = (char *)realloc(*buffer, (MAXMSG - 1) * (i + 1) + 1);
    }
    nbytes = recv(filedes, (*buffer + read_bytes), MAXMSG - 1, 0);
    ++i;
    printf("\tcalling..............\n");
    if (nbytes > 0) {
      read_bytes += nbytes;
    }
  } while (nbytes > 0);

  printf("\tnbytes.............. %d\n", nbytes);
  *buffer_size = read_bytes;
  return read_bytes;
}
