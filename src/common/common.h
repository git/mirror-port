/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMMON_H
#define COMMON_H

#define _GNU_SOURCE
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "base64.h"

#define DEBUG 1

/* server */
#define SERVER_EXTERNAL_LISTEN_PORT 5555
#define SERVER_CLIENT_LISTEN_PORT 5556

/* client */
#define LOCAL_SERVER_PORT 80
#define LOCAL_SERVER_HOST "localhost"

#define MAXMSG 8192 * 8

#define MAX_CONNECTIONS 7

struct Mirror_header {
  int req_from_server_remote_socket_id;
  int buffer_size;
};

struct Mirror_map {
  int req_to_local_socket_id;
  int req_from_server_remote_socket_id;
};

int splice_all(int from, int to, long long bytes);
int transfer(int from, int to, long long bytes);
int read_from_socket(int filedes, char *buffer, int *buffer_size);
int read_from_socket2(int filedes, char **buffer, int *buffer_size);

#endif
