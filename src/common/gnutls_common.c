/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gnutls_common.h"
#include <signal.h>
#include <unistd.h>

#define LOOP_CHECK(rval, cmd)                                                  \
  do {                                                                         \
    rval = cmd;                                                                \
  } while (rval == GNUTLS_E_AGAIN || rval == GNUTLS_E_INTERRUPTED)

int encode_base64_and_send(
    gnutls_session_t *gnutls_session, void *buffer, int buffer_size,
    int req_from_server_remote_socket_id /* 0 for client message */) {
  int b64enc_buffer_size = b64e_size(buffer_size);
  void *b64enc_buffer = (char *)malloc(b64enc_buffer_size + 1);
  memset(b64enc_buffer, 0, (b64enc_buffer_size + 1));

  b64enc_buffer_size = b64_encode(buffer, buffer_size, b64enc_buffer);

  struct Mirror_header mirror_header;
  mirror_header.req_from_server_remote_socket_id =
      req_from_server_remote_socket_id;
  mirror_header.buffer_size = b64enc_buffer_size;
  // send size
  int ret = gnutls_record_send(*gnutls_session, (void *)&mirror_header,
                               sizeof(struct Mirror_header));
#if DEBUG
  printf("\toutgoing size %d\n", b64enc_buffer_size);
#endif
  long int send_bytes = 0;

  while (1) {
    int send_size = (b64enc_buffer_size - send_bytes) > MAXMSG
                        ? MAXMSG
                        : (b64enc_buffer_size - send_bytes);
    LOOP_CHECK(ret, gnutls_record_send(*gnutls_session,
                                       b64enc_buffer + send_bytes, send_size));
#if DEBUG
    printf("\tsend status ret %d\n", ret);
#endif
    if (ret > 0) {
      send_bytes += ret;
    } else if (ret < 0) {
      printf("\twriting error in gnutls_record_send\n");
      break;
    }

    if (send_bytes == b64enc_buffer_size) {
#if DEBUG
      printf("\ttotal sent %d\n", send_bytes);
#endif
      break;
    }
  }

  free(b64enc_buffer);
  return send_bytes;
}

int decode_base64_and_send(int sock, void *buffer, int buffer_size) {
  signal(SIGPIPE, SIG_IGN);
  int b64dec_buffer_size = b64d_size(buffer_size);
#if DEBUG
  printf("\tbuffer_size: %d\n", buffer_size);
#endif
  void *b64dec_buffer = (char *)malloc(b64dec_buffer_size + 1);
  memset(b64dec_buffer, 0, (b64dec_buffer_size + 1));

  b64dec_buffer_size = b64_decode(buffer, buffer_size, b64dec_buffer);
#if DEBUG
  printf("\tbuffer_size: %d\n", buffer_size);
  printf("\tb64dec_buffer_size: %d\n", b64dec_buffer_size);
#endif
  int ret;
  long int send_bytes = 0;

  while (1) {
#if DEBUG
    printf("\tsending raw data...\n");
#endif
    int send_size = (b64dec_buffer_size - send_bytes) > MAXMSG
                        ? MAXMSG
                        : (b64dec_buffer_size - send_bytes);
    ret = send(sock, b64dec_buffer + send_bytes, send_size, 0);
#if DEBUG
    printf("\tsend raw status ret %d\n", ret);
#endif
    if (ret > 0) {
      send_bytes += ret;
    } else if (ret < 0) {
      printf("\twriting error in write\n");
      break;
    }

    if (send_bytes == b64dec_buffer_size) {
#if DEBUG
      printf("\ttotal raw sent %d\n", send_bytes);
#endif
      break;
    }
  }
  free(b64dec_buffer);
  return send_bytes;
}

int read_data_from_gnutls_session(int sock, gnutls_session_t *gnutls_session,
                                  void **buffer, char *description,
                                  int *req_from_server_remote_socket_id,
                                  int close_on_error) {
  memset(*buffer, 0, MAXMSG + 1);

  int ret;
  struct Mirror_header mirror_header;
  ret = gnutls_record_recv(*gnutls_session, &mirror_header,
                           sizeof(struct Mirror_header));
  int incomming_size = mirror_header.buffer_size;
  *req_from_server_remote_socket_id =
      mirror_header.req_from_server_remote_socket_id;
#if DEBUG
  printf("\tret %d with incomming size %d\n", ret, incomming_size);
#endif
  if (incomming_size > MAXMSG) {
    *buffer = (char *)realloc(*buffer, incomming_size + 1);
    memset(*buffer, MAXMSG + 1, incomming_size + 1);
  }
  // for debugging
  if (ret <= 0) {
    // pending check why we are receiving this data
    LOOP_CHECK(ret,
               gnutls_record_recv(*gnutls_session, *buffer, incomming_size));

    printf("\tdebugging error: new receive data ret %d data %s\n", ret,
           *buffer);

    // temporary fix
    incomming_size = MAXMSG;
  }

  int nbytes;
  long int read_bytes = 0;
  while (1) {
    int send_size = (incomming_size - read_bytes) > MAXMSG
                        ? MAXMSG
                        : (incomming_size - read_bytes);
#if DEBUG
    printf("\twaiting for data already read %d\n", read_bytes);
#endif
    ret = gnutls_record_recv(*gnutls_session, *buffer + read_bytes, send_size);
    if (ret == 0) {
      printf("- Peer has closed the TLS connection\n");
      // goto end;
    } else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
      fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));
      fprintf(stderr, "\tclosing %s socket %d\n", description, sock);
      close(sock);
      break;
    } else if (ret < 0) {
      fprintf(stderr, "*** Error: %s\n", gnutls_strerror(ret));
      // if (close_on_error) {
      fprintf(stderr, "\tclosing %s socket %d\n", description, sock);
      close(sock);
      break;
      //}
    }
    read_bytes += ret;
    if (read_bytes == incomming_size) {
      ret = incomming_size;
#if DEBUG
      printf("\tfinished reading %d\n", read_bytes);
#endif
      break;
    }
  }
  // LOOP_CHECK(ret, gnutls_record_recv(*gnutls_session, buffer,
  // incomming_size));

  return ret;
}
