/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "server_common.h"

void get_socket_details(int sock, char *msg) {
  struct sockaddr_in6 clientaddr;
  int addrlen = sizeof(clientaddr);
  char str[INET6_ADDRSTRLEN];
  if (getpeername(sock, (struct sockaddr *)&clientaddr, &addrlen)) {
    perror("getpeername() failed");
  } else {
    if (inet_ntop(AF_INET6, &clientaddr.sin6_addr, str, sizeof(str))) {
      fprintf(stderr,
              "Server: connect from %s host %s, port %d. on "
              "socket %d\n",
              msg, str, ntohs(clientaddr.sin6_port), sock);
    }
  }
}

int make_socket(uint16_t port) {
  int sock;
  sock = socket(PF_INET6, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("socket");
    exit(EXIT_FAILURE);
  }

  int result = 0;
  int status = 1;
  result = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&status,
                      sizeof(status));
  if (result < 0) {
    perror("setsockopt() failed for SO_REUSEADDR");
    close(sock);
    exit(-1);
  }

  result = 0;
  result = ioctl(sock, FIONBIO, (char *)&status);
  if (result < 0) {
    perror("ioctl() failed for FIONBIO");
    close(sock);
    exit(-1);
  }

  result = 0;
  status = 0;
  result = setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (char *)&status,
                      sizeof(status));
  if (result < 0) {
    perror("setsockopt() failed for IPV6_V6ONLY");
    close(sock);
    exit(-1);
  }

  struct timeval timeout;
  timeout.tv_sec = 30; // seconds
  timeout.tv_usec = 0;
  if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
                 sizeof(timeout)) < 0) {
    perror("setsockopt() failed for SO_SNDTIMEO");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in6 addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin6_family = AF_INET6;
  memcpy(&addr.sin6_addr, &in6addr_any, sizeof(in6addr_any));
  addr.sin6_port = htons(port);
  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    perror("bind");
    exit(EXIT_FAILURE);
  }

  return sock;
}
