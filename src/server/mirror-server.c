/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "../common/gnutls_common.h"

#include "gnutls_server.h"
#include "server_common.h"

void check_auth_token(gnutls_session_t *local_client_session,
                      int *local_client_sock, int i, fd_set *active_fd_set,
                      char *secret_token, int *local_client_authenticated,
                      char *buffer) {
  fprintf(stderr, "\tinitial message from client\n");
  if (strcmp(secret_token, buffer) != 0) {
    gnutls_deinit(*local_client_session);
    close(i);
    FD_CLR(i, active_fd_set);
    *local_client_sock = 0;
    fprintf(stderr, "\tinvalid secret token: %s\n", buffer);
  } else {
    fprintf(stderr, "\tvalid secret token found\n", buffer);
    *local_client_authenticated = 1;
  }
}

void handle_auth_and_empty_remote_conn(gnutls_session_t *local_client_session,
                                       int *local_client_sock, int i,
                                       fd_set *active_fd_set,
                                       char *secret_token,
                                       int *local_client_authenticated,
                                       char *buffer, int buffer_size) {
  int b64dec_buffer_size = b64d_size(buffer_size);
  char *b64dec_buffer = (char *)malloc(b64dec_buffer_size + 1);
  memset(b64dec_buffer, 0, (b64dec_buffer_size + 1));
  b64dec_buffer_size = b64_decode(buffer, buffer_size, b64dec_buffer);

  if (*local_client_authenticated == 0) {
    check_auth_token(local_client_session, local_client_sock, i, active_fd_set,
                     secret_token, local_client_authenticated, b64dec_buffer);
  } else {
    // local client is trying to send message to a remote client
    fprintf(stderr,
            "\treceived message from local client %d but remote "
            "client is not connected\n",
            *local_client_sock);
    // fprintf(stderr, "\tb64dec_buffer: %s ", b64dec_buffer);
  }
}

void handle_remote_socket(gnutls_session_t *local_client_session,
                          int server_external_socket, int local_client_sock,
                          int *no_of_accepted_conn, int i,
                          fd_set *active_fd_set) {
#if DEBUG
  fprintf(stderr, "receiving data from remote socket %d\n", i);
  fprintf(stderr, "\ttransfering from remote %d -> client %d \n", i,
          local_client_sock);
#endif
  char *buffer = (char *)malloc(MAXMSG + 1);
  int buffer_size = 0;
  int status = read_from_socket(i, buffer, &buffer_size);
  if (status > 0) {
    encode_base64_and_send(local_client_session, buffer, buffer_size, i);
  } else if (status <= 0) {
#if DEBUG
    fprintf(stderr, "\tclosing remote connection socket %d with status %d\n", i,
            status);
#endif
    close(i);
    FD_CLR(i, active_fd_set);

    if (*no_of_accepted_conn == MAX_CONNECTIONS) {
      FD_SET(server_external_socket, active_fd_set);
    }
    // note: the following order might be required
    --(*no_of_accepted_conn);
  }
  free(buffer);
}

void handle_local_client_socket(gnutls_session_t *local_client_session,
                                int *local_client_sock,
                                int server_external_socket, int i,
                                fd_set *active_fd_set, char *secret_token,
                                int *local_client_authenticated,
                                int *no_of_accepted_conn) {
#if DEBUG
  fprintf(stderr, "receiving data from client socket %d\n", i);
#endif
  char *buffer = (char *)malloc(MAXMSG + 1);
  int close_on_error = 0;
  // if (*remote_client_sock_count == 0) {
  //  close_on_error = 0;
  //}
  // default 0 for client connection
  int req_from_server_remote_socket_id;
  int ret = read_data_from_gnutls_session(
      i, local_client_session, (void **)&buffer, "client connection",
      &req_from_server_remote_socket_id, close_on_error);

  // pending validate req_from_server_remote_socket_id

  /* 4 = manual EOF length in base64*/
  /*
  if (req_from_server_remote_socket_id != 0 && ret == 4) {
    int buffer_size = ret;
    int b64dec_buffer_size = b64d_size(buffer_size);
    char *b64dec_buffer = (char *)malloc(b64dec_buffer_size + 1);
    memset(b64dec_buffer, 0, (b64dec_buffer_size + 1));
    b64dec_buffer_size = b64_decode(buffer, buffer_size, b64dec_buffer);

    if (b64dec_buffer[0] == 'E' && b64dec_buffer[1] == 'O' &&
        b64dec_buffer[2] == 'F') {
      fprintf(stderr, "\tclosing remote connection %d due to EOF\n",
              req_from_server_remote_socket_id);

      char *test = "HTTP/1.1 200 OK\r\nConnection: close\r\n\r\n";
      int test_status = send(req_from_server_remote_socket_id, test, strlen(test), 0);
      printf("\ttest_status %d\n", test_status);

      close(req_from_server_remote_socket_id);
      FD_CLR(req_from_server_remote_socket_id, active_fd_set);

      if (*no_of_accepted_conn == MAX_CONNECTIONS) {
        FD_SET(server_external_socket, active_fd_set);
      }
      --(*no_of_accepted_conn);
      free(buffer);
      return;
    }
  }
  */
  if (ret < 0) {
    fprintf(stderr, "client closed connection %d\n", i);
    FD_CLR(i, active_fd_set);
    *local_client_sock = 0;
    *local_client_authenticated = 0;
  } else if (ret > 0) {
    int buffer_size = ret;
    if (req_from_server_remote_socket_id == 0) {
      // fprintf(stderr, "%s\n", buffer);
      handle_auth_and_empty_remote_conn(
          local_client_session, local_client_sock, i, active_fd_set,
          secret_token, local_client_authenticated, buffer, buffer_size);
    } else {
#if DEBUG
      fprintf(stderr, "\ttransfering from client %d -> remote %d \n",
              *local_client_sock, req_from_server_remote_socket_id);
#endif
      // decode base64
      int status = decode_base64_and_send(req_from_server_remote_socket_id,
                                          buffer, buffer_size);
      if (status < 0) {
#if DEBUG
        fprintf(stderr, "\ttesting **** client connection %d\n",
                *local_client_sock);
#endif
      }
    }
  }
  free(buffer);
}

void handle_server_client_socket(gnutls_certificate_credentials_t *x509_cred,
                                 gnutls_priority_t *priority_cache,
                                 gnutls_session_t *local_client_session,
                                 int server_client_socket,
                                 int *local_client_sock, int i,
                                 fd_set *active_fd_set) {
  gnutls_init_client(x509_cred, priority_cache, local_client_session);
  int new_client_socket = accept(server_client_socket, NULL, NULL);
  if (new_client_socket < 0) {
    perror("accept");
    exit(EXIT_FAILURE);
  }
  get_socket_details(new_client_socket, "client");

  gnutls_transport_set_int(*local_client_session, new_client_socket);
  int ret;
  LOOP_CHECK(ret, gnutls_handshake(*local_client_session));
  if (ret < 0) {
    close(new_client_socket);
    gnutls_deinit(*local_client_session);
    fprintf(stderr, "*** Handshake has failed (%s)\n\n", gnutls_strerror(ret));
    return;
  }

  printf("- Handshake was completed\n");

  if (*local_client_sock != 0) {
    char *hello = "another client already connected please try again later\n";
    // pending check base64
    send(new_client_socket, hello, strlen(hello), 0);
    fprintf(stderr, "\tclosing client connection socket %d\n", i);
    close(new_client_socket);
  } else {
    *local_client_sock = new_client_socket;
    FD_SET(new_client_socket, active_fd_set);
  }
}

void handle_external_socket(int i, int server_external_socket,
                            int *no_of_accepted_conn, fd_set *active_fd_set) {
  if (*no_of_accepted_conn < MAX_CONNECTIONS) {
    //*no_of_accepted_conn = 0;
    int new_socket = accept(server_external_socket, NULL, NULL);
    if (new_socket < 0) {
      perror("accept");
      exit(EXIT_FAILURE);
    }
    get_socket_details(new_socket, "remote");
    FD_SET(new_socket, active_fd_set);

    ++(*no_of_accepted_conn);
    if (*no_of_accepted_conn == MAX_CONNECTIONS) {
      FD_CLR(server_external_socket, active_fd_set);
    }
  } else {
    // currently not in use as we used FD_CLR after new connection
    fprintf(stderr, "Server: delaying new connection\n");
  }
}

int init_server_sockets(int *server_external_socket,
                        int *server_client_socket) {
  *server_external_socket = make_socket(SERVER_EXTERNAL_LISTEN_PORT);
  if (listen(*server_external_socket, 1) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }
  *server_client_socket = make_socket(SERVER_CLIENT_LISTEN_PORT);
  if (listen(*server_client_socket, 1) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
  char *secret_token = NULL;
  if (argc == 2) {
    secret_token = argv[1];
  } else {
    printf("Please provide a secret token for client authorization\n");
    printf("./mirror-server SECRET_TOKEN\n");
    return -1;
  }

  gnutls_certificate_credentials_t x509_cred;
  gnutls_priority_t priority_cache;
  gnutls_session_t local_client_session;
  init_gnutls(&x509_cred, &priority_cache);

  int server_external_socket;
  int server_client_socket;
  if (init_server_sockets(&server_external_socket, &server_client_socket) !=
      EXIT_SUCCESS) {
    exit(EXIT_FAILURE);
  }

  fd_set active_fd_set, read_fd_set;
  FD_ZERO(&active_fd_set);
  FD_SET(server_external_socket, &active_fd_set);
  FD_SET(server_client_socket, &active_fd_set);

  int local_client_sock = 0;
  int no_of_accepted_conn = 1;

  int local_client_authenticated = 0;

  while (1) {
    /* Block until input arrives on one or more active sockets. */
    read_fd_set = active_fd_set;
    if (select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
      perror("select");
      exit(EXIT_FAILURE);
    }

    /* Service all the sockets with input pending. */
    for (int i = 0; i < FD_SETSIZE; ++i) {
      if (FD_ISSET(i, &read_fd_set)) {
        if (i == server_external_socket) {
          /* Connection request on original socket. */
          handle_external_socket(i, server_external_socket,
                                 &no_of_accepted_conn, &active_fd_set);
        } else if (i == server_client_socket) {
          handle_server_client_socket(
              &x509_cred, &priority_cache, &local_client_session,
              server_client_socket, &local_client_sock, i, &active_fd_set);
        } else if (i == local_client_sock) {
          handle_local_client_socket(&local_client_session, &local_client_sock,
                                     server_external_socket, i, &active_fd_set,
                                     secret_token, &local_client_authenticated,
                                     &no_of_accepted_conn);
        } else {
          /* Data arriving on an already-connected socket. */
          handle_remote_socket(&local_client_session, server_external_socket,
                               local_client_sock, &no_of_accepted_conn, i,
                               &active_fd_set);
        }
      }
    }
  }

  close(server_client_socket);
  gnutls_certificate_free_credentials(x509_cred);
  gnutls_priority_deinit(priority_cache);
  gnutls_global_deinit();
}
