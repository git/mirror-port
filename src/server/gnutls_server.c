/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gnutls_server.h"

void init_gnutls(gnutls_certificate_credentials_t *x509_cred,
                 gnutls_priority_t *priority_cache) {
  /* for backwards compatibility with gnutls < 3.3.0 */
  CHECK(gnutls_global_init());
  CHECK(gnutls_certificate_allocate_credentials(x509_cred));
  CHECK(gnutls_certificate_set_x509_trust_file(*x509_cred, CAFILE,
                                               GNUTLS_X509_FMT_PEM));
  CHECK(gnutls_certificate_set_x509_crl_file(*x509_cred, CRLFILE,
                                             GNUTLS_X509_FMT_PEM));
  /* The following code sets the certificate key pair as well as,
   * an OCSP response which corresponds to it. It is possible
   * to set multiple key-pairs and multiple OCSP status responses
   * (the latter since 3.5.6). See the manual pages of the individual
   * functions for more information.
   */
  CHECK(gnutls_certificate_set_x509_key_file(*x509_cred, CERTFILE, KEYFILE,
                                             GNUTLS_X509_FMT_PEM));
  /*
  CHECK(gnutls_certificate_set_ocsp_status_request_file(x509_cred,
                                                        OCSP_STATUS_FILE,
                                                        0));
  */
  CHECK(gnutls_priority_init(priority_cache, NULL, NULL));
  /* Instead of the default options as shown above one could specify
   * additional options such as server precedence in ciphersuite selection
   * as follows:
   * gnutls_priority_init2(&priority_cache,
   *                       "%SERVER_PRECEDENCE",
   *                       NULL, GNUTLS_PRIORITY_INIT_DEF_APPEND);
   */
#if GNUTLS_VERSION_NUMBER >= 0x030506
  /* only available since GnuTLS 3.5.6, on previous versions see
   * gnutls_certificate_set_dh_params(). */
  gnutls_certificate_set_known_dh_params(*x509_cred, GNUTLS_SEC_PARAM_MEDIUM);
#endif
}

void gnutls_init_client(gnutls_certificate_credentials_t *x509_cred,
                        gnutls_priority_t *priority_cache,
                        gnutls_session_t *local_client_session) {

  CHECK(gnutls_init(local_client_session, GNUTLS_SERVER));
  CHECK(gnutls_priority_set(*local_client_session, *priority_cache));
  CHECK(gnutls_credentials_set(*local_client_session, GNUTLS_CRD_CERTIFICATE,
                               *x509_cred));

  /* We don't request any certificate from the client.
   * If we did we would need to verify it. One way of
   * doing that is shown in the "Verifying a certificate"
   * example.
   */
  gnutls_certificate_server_set_request(*local_client_session,
                                        GNUTLS_CERT_IGNORE);
  gnutls_handshake_set_timeout(*local_client_session,
                               GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
}
