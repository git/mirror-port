/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <arpa/inet.h>
#include <assert.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "../common/base64.h"
#include "../common/gnutls_common.h"

#include "client_common.h"
#include "gnutls_client.h"

int init_mirror_map(struct Mirror_map *mirror_map) {
  for (int i = 0; i < MAX_CONNECTIONS; ++i) {
    mirror_map[i].req_to_local_socket_id = 0;
    mirror_map[i].req_from_server_remote_socket_id = 0;
  }
}

int get_mirror_map_from_remote_sock(struct Mirror_map *mirror_map,
                                    int req_from_server_remote_socket_id) {
  for (int i = 0; i < MAX_CONNECTIONS; ++i) {
    if (mirror_map[i].req_from_server_remote_socket_id ==
        req_from_server_remote_socket_id)
      return i;
  }
  return -1;
}

int get_mirror_map_from_local_sock(struct Mirror_map *mirror_map,
                                   int req_to_local_socket_id) {
  for (int i = 0; i < MAX_CONNECTIONS; ++i) {
    if (mirror_map[i].req_to_local_socket_id == req_to_local_socket_id)
      return i;
  }
  return -1;
}

int get_free_mirror_map(struct Mirror_map *mirror_map) {
  for (int i = 0; i < MAX_CONNECTIONS; ++i) {
    if (mirror_map[i].req_to_local_socket_id == 0 &&
        mirror_map[i].req_from_server_remote_socket_id == 0)
      return i;
  }
  return -1;
}

int print_mirror_map(struct Mirror_map *mirror_map) {
  printf("\tmap:\n");
  for (int i = 0; i < MAX_CONNECTIONS; ++i) {
    printf("\t%d %d %d\n", i, mirror_map[i].req_to_local_socket_id,
           mirror_map[i].req_from_server_remote_socket_id);
  }
}

void handle_local_server_socket(int i, int remote_server_socket,
                                gnutls_session_t *remote_session,
                                fd_set *active_fd_set,
                                struct Mirror_map *mirror_map) {
#if DEBUG
  fprintf(stderr, "receiving data from local server socket %d\n", i);
  fprintf(stderr, "\ttransfering from local %d -> remote %d \n", i,
          remote_server_socket);
#endif
  int req_from_server_remote_socket_id = 0;

  int mirror_map_position_from_local =
      get_mirror_map_from_local_sock(mirror_map, i);
  if (mirror_map_position_from_local != -1) {
    req_from_server_remote_socket_id =
        mirror_map[mirror_map_position_from_local]
            .req_from_server_remote_socket_id;
  } else {
    fprintf(stderr,
            "\tcould not find req_from_server_remote_socket_id for socket %d\n",
            i);
  }

  char *buffer = (char *)malloc(MAXMSG + 1);
  memset(buffer, 0, sizeof(buffer));
  int buffer_size;
  int status = read_from_socket(i, buffer, &buffer_size);
  if (status > 0) {
    encode_base64_and_send(remote_session, buffer, buffer_size,
                           req_from_server_remote_socket_id);
  } else if (status == 0) {
    fprintf(stderr, "\tclosing local server socket %d for remote req %d\n", i,
            req_from_server_remote_socket_id);
    close(i);
    FD_CLR(i, active_fd_set);
    /*
    char *eof_buffer = "EOF";
    int eof_size = 3;
    encode_base64_and_send(remote_session, eof_buffer, eof_size,
                           req_from_server_remote_socket_id);
    */
    if (mirror_map_position_from_local != -1) {
      mirror_map[mirror_map_position_from_local].req_to_local_socket_id = 0;
      mirror_map[mirror_map_position_from_local]
          .req_from_server_remote_socket_id = 0;
    } else {
      fprintf(stderr, "\terror in local server socket %d for remote req %d\n", i,
	      req_from_server_remote_socket_id);
    }

    print_mirror_map(mirror_map);
  }
  free(buffer);
}

void handle_remote_server_socket(int remote_server_socket,
                                 gnutls_session_t *remote_session, int use_ipv6,
                                 fd_set *active_fd_set,
                                 struct Mirror_map *mirror_map) {
  fprintf(stderr, "\nreceiving data from remote server socket %d\n",
          remote_server_socket);
  char *buffer = (char *)malloc(MAXMSG + 1);
  int close_on_error = 1;
  int req_from_server_remote_socket_id = 0;
  int ret = read_data_from_gnutls_session(
      remote_server_socket, remote_session, (void **)&buffer, "remote server",
      &req_from_server_remote_socket_id, close_on_error);
  if (ret > 0) {
    int buffer_size = ret;
    int status = 0;

    int local_server_sock_new = 0;

    int existing_local_server_socket_idx = get_mirror_map_from_remote_sock(
        mirror_map, req_from_server_remote_socket_id);
#if DEBUG
    printf("\texisting_local_server_socket_index: %d\n",
           existing_local_server_socket_idx);
#endif
    if (existing_local_server_socket_idx == -1) { // no map found
      int new_local_server_sock = create_local_server_socket(&status, use_ipv6);
      if (!status) {
        fprintf(stderr, "could not create local server socket %d\n",
                remote_server_socket);
        exit(EXIT_FAILURE);
      }
      // set value to map
      int free_map_index = get_free_mirror_map(mirror_map);
#if DEBUG
      printf("\tfree_map_index: %d\n", free_map_index);
#endif
      if (free_map_index < 0) {
        fprintf(stderr, "error**** could not find free map\n");
        return;
      }
      mirror_map[free_map_index].req_to_local_socket_id = new_local_server_sock;
      mirror_map[free_map_index].req_from_server_remote_socket_id =
          req_from_server_remote_socket_id;

      printf("created local_server_socket: %d\n", new_local_server_sock);
      FD_SET(new_local_server_sock, active_fd_set);

      local_server_sock_new = new_local_server_sock;
    } else {
      local_server_sock_new =
          mirror_map[existing_local_server_socket_idx].req_to_local_socket_id;
    }

#if DEBUG
    printf("\tforwarding data to local server\n");
#endif
    int result =
        decode_base64_and_send(local_server_sock_new, buffer, buffer_size);
    // pending check status
  }
  free(buffer);
}

int main(int argc, char *argv[]) {
  char *remote_server_address = NULL;
  int remote_server_port = 0;
  char *secret_token = NULL;

  if (argc == 4) {
    remote_server_address = argv[1];
    remote_server_port = atoi(argv[2]);
    secret_token = argv[3];
  } else {
    printf("Please provide server IP and PORT\n");
    printf("./mirror-client SERVER_IP PORT SECRET_TOKEN\n");
    return -1;
  }

  int use_ipv6 = 0;

  int ret;
  gnutls_session_t remote_session;
  gnutls_certificate_credentials_t xcred;

  init_gnutls_client(&remote_session, &xcred);

  int remote_server_socket;
  connect_to_server(&remote_server_socket, remote_server_address,
                    remote_server_port, use_ipv6);

  gnutls_transport_set_int(remote_session, remote_server_socket);
  gnutls_handshake_set_timeout(remote_session,
                               GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

  fd_set active_fd_set, read_fd_set;
  FD_ZERO(&active_fd_set);
  FD_SET(remote_server_socket, &active_fd_set);

  struct Mirror_map mirror_map[MAX_CONNECTIONS];
  init_mirror_map(mirror_map);

  handshake(&remote_session, &xcred);

  fprintf(stderr, "\nsending hello to remote server socket %d\n",
          remote_server_socket);
  /*
  int tokenSize = strlen(secret_token)*sizeof(char);
  ret = gnutls_record_send(remote_session, (void*)&tokenSize, sizeof(int));
  ret = gnutls_record_send(remote_session, secret_token,
                           tokenSize);
  */
  ret = encode_base64_and_send(&remote_session, secret_token,
                               strlen(secret_token) * sizeof(char), 0);

  while (1) {
    /* Block until input arrives on one or more active sockets. */
    read_fd_set = active_fd_set;

    if (select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
      perror("select");
      exit(EXIT_FAILURE);
    }

    /* Service all the sockets with input pending. */
    for (int i = 0; i < FD_SETSIZE; ++i) {
      if (FD_ISSET(i, &read_fd_set)) {
        if (i == remote_server_socket) {
          handle_remote_server_socket(remote_server_socket, &remote_session,
                                      use_ipv6, &active_fd_set, mirror_map);
        } else {
          handle_local_server_socket(i, remote_server_socket, &remote_session,
                                     &active_fd_set, mirror_map);
        }
      }
    }
  }

  gnutls_deinit(remote_session);
  gnutls_certificate_free_credentials(xcred);
  gnutls_global_deinit();
  return 0;
}
