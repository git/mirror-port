/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "client_common.h"

void init_sockaddr_ipv4(struct sockaddr_in *name, const char *hostname,
                        uint16_t port) {
  struct hostent *hostinfo;
  name->sin_family = AF_INET;
  name->sin_port = htons(port);
  hostinfo = gethostbyname(hostname);
  if (hostinfo == NULL) {
    fprintf(stderr, "Unknown host %s.\n", hostname);
    exit(EXIT_FAILURE);
  }
  name->sin_addr = *(struct in_addr *)hostinfo->h_addr;
}

void init_sockaddr_ipv6(struct sockaddr_in6 *name, const char *hostname,
                        uint16_t port) {
  struct hostent *hostinfo;
  memset((char *)name, 0, sizeof(*name));
  name->sin6_family = AF_INET6;
  name->sin6_port = htons(port);
  // name->sin6_flowinfo = 0;
  hostinfo = gethostbyname2(hostname, AF_INET6);
  if (hostinfo == NULL) {
    fprintf(stderr, "Unknown host %s.\n", hostname);
    exit(EXIT_FAILURE);
  }
  memmove((char *)name->sin6_addr.s6_addr, (char *)hostinfo->h_addr,
          hostinfo->h_length);
}

void write_to_server(int socket, char *message) {
  int nbytes;
  // fix sizeof(message) ****************+
  nbytes = send(socket, message, sizeof(message), 0);
  if (nbytes < 0) {
    perror("write");
    exit(EXIT_FAILURE);
  }
}

int create_local_server_socket(int *status, int use_ipv6) {
  *status = 0;
  int local_server_socket;
  if (use_ipv6) {
    if ((local_server_socket = socket(AF_INET6, SOCK_STREAM, 0)) < 0) {
      printf("\n Socket creation error \n");
      return -1;
    }
    struct sockaddr_in6 servername;
    init_sockaddr_ipv6(&servername, LOCAL_SERVER_HOST, LOCAL_SERVER_PORT);
    if (0 > connect(local_server_socket, (struct sockaddr *)&servername,
                    sizeof(servername))) {
      perror("connect (local_server_socket)");
      exit(EXIT_FAILURE);
    }
  } else {
    if ((local_server_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      printf("\n Socket creation error \n");
      return -1;
    }
    struct sockaddr_in servername;
    init_sockaddr_ipv4(&servername, LOCAL_SERVER_HOST, LOCAL_SERVER_PORT);
    if (0 > connect(local_server_socket, (struct sockaddr *)&servername,
                    sizeof(servername))) {
      perror("connect (local_server_socket)");
      exit(EXIT_FAILURE);
    }
  }

  *status = 1;
  return local_server_socket;
}

int connect_to_server(int *remote_server_socket, char *remote_server_host, int remote_server_port, int use_ipv6) {
  if (use_ipv6) {
    fprintf(stderr, "\nInfo: using IPV6\n");
    *remote_server_socket = socket(AF_INET6, SOCK_STREAM, 0);
    if (*remote_server_socket < 0) {
      perror("socket (client)");
      exit(EXIT_FAILURE);
    }
    struct sockaddr_in6 servername;
    init_sockaddr_ipv6(&servername, remote_server_host, remote_server_port);
    if (0 > connect(*remote_server_socket, (struct sockaddr *)&servername,
                    sizeof(servername))) {
      perror("unable to connect to server");
      exit(EXIT_FAILURE);
    }
  } else {
    fprintf(stderr, "\nInfo: using IPV4\n");
    *remote_server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (*remote_server_socket < 0) {
      perror("socket (client)");
      exit(EXIT_FAILURE);
    }
    struct sockaddr_in servername;
    init_sockaddr_ipv4(&servername, remote_server_host, remote_server_port);
    if (0 > connect(*remote_server_socket, (struct sockaddr *)&servername,
                    sizeof(servername))) {
      perror("unable to connect to server");
      exit(EXIT_FAILURE);
    }
  }

  struct timeval tv;
  tv.tv_sec = 20; /* 20 Secs Timeout */
  tv.tv_usec = 0;
  if (setsockopt(*remote_server_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv,
                 sizeof(tv)) < 0) {
    fprintf(stderr, "\nTime out for remote socket %d\n", *remote_server_socket);
    exit(EXIT_FAILURE);
  }

  return 0;
}
