/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gnutls_client.h"

void init_gnutls_client(gnutls_session_t *gnutls_session,
                        gnutls_certificate_credentials_t *xcred) {
  if (gnutls_check_version("3.4.6") == NULL) {
    fprintf(stderr, "GnuTLS 3.4.6 or later is required for this example\n");
    exit(1);
  }

  /* for backwards compatibility with gnutls < 3.3.0 */
  CHECK(gnutls_global_init());
  /* X509 stuff */
  CHECK(gnutls_certificate_allocate_credentials(xcred));
  /* sets the system trusted CAs for Internet PKI */
  CHECK(gnutls_certificate_set_x509_system_trust(*xcred));
  /* If client holds a certificate it can be set using the following:
   */
  //gnutls_certificate_set_x509_key_file(*xcred, "cert/ca-cert.pem",
  //                                     "cert/key.pem", GNUTLS_X509_FMT_PEM);
  /* Initialize TLS gnutls_session */
  CHECK(gnutls_init(gnutls_session, GNUTLS_CLIENT));
  CHECK(gnutls_server_name_set(*gnutls_session, GNUTLS_NAME_DNS, "localhost",
                               strlen("localhost")));
  /* It is recommended to use the default priorities */
  CHECK(gnutls_set_default_priority(*gnutls_session));
  /* put the x509 credentials to the current gnutls_session
   */
  CHECK(
      gnutls_credentials_set(*gnutls_session, GNUTLS_CRD_CERTIFICATE, *xcred));
  gnutls_session_set_verify_cert(*gnutls_session, "localhost", 0);
}

void handshake(gnutls_session_t *gnutls_session,
               gnutls_certificate_credentials_t *xcred) {
  /* Perform the TLS handshake
   */
  int ret;
  char *desc;
  int type;
  unsigned status;
  gnutls_datum_t out;

  do {
    ret = gnutls_handshake(*gnutls_session);
  } while (ret < 0 && gnutls_error_is_fatal(ret) == 0);
  if (ret < 0) {
    if (ret == GNUTLS_E_CERTIFICATE_VERIFICATION_ERROR) {
      /* check certificate verification status */
      type = gnutls_certificate_type_get(*gnutls_session);
      status = gnutls_session_get_verify_cert_status(*gnutls_session);
      CHECK(
          gnutls_certificate_verification_status_print(status, type, &out, 0));
      printf("cert verify output: %s\n", out.data);
      gnutls_free(out.data);
    }
    fprintf(stderr, "*** Handshake failed: %s\n", gnutls_strerror(ret));

    gnutls_deinit(*gnutls_session);
    gnutls_certificate_free_credentials(*xcred);
    gnutls_global_deinit();

  } else {
    desc = gnutls_session_get_desc(*gnutls_session);
    printf("- Session info: %s\n", desc);
    gnutls_free(desc);
  }
}
