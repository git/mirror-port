/*
    mirror-port is a program to mirror your local port to a remote server.
    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../common/common.h"

void init_sockaddr_ipv4(struct sockaddr_in *name, const char *hostname,
                        uint16_t port);
void init_sockaddr_ipv6(struct sockaddr_in6 *name, const char *hostname,
                        uint16_t port);
void write_to_server(int socket, char *message);
int create_local_server_socket(int *status, int use_ipv6);
int connect_to_server(int *remote_server_socket, char *remote_server_host, int remote_server_port, int use_ipv6);
