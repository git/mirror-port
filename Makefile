#    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

CC=gcc
CCFLAGS=-g -lgnutls
RM=rm -f
SRC_DIR=src
SRC_COMMON=common
SRC_SERVER=server
SRC_CLIENT=client
BUILD_DIR=build
OBJ_DIR=$(BUILD_DIR)/objects

all: common base64 server_common gnutls_common gnutls_server mirror-server client_common gnutls_client mirror-client

common: $(SRC_DIR)/$(SRC_COMMON)/common.c
	$(CC) -o $(OBJ_DIR)/$@.o -c $? $(CCFLAGS)

base64: $(SRC_DIR)/$(SRC_COMMON)/base64.c
	$(CC) -o $(OBJ_DIR)/$@.o -c $? $(CCFLAGS)

gnutls_common: $(SRC_DIR)/$(SRC_COMMON)/gnutls_common.c
	$(CC) -o $(OBJ_DIR)/$@.o -c $? $(CCFLAGS)

server_common: $(SRC_DIR)/$(SRC_SERVER)/server_common.c
	$(CC) -o $(OBJ_DIR)/$@.o -c $? $(CCFLAGS)

gnutls_server: $(SRC_DIR)/$(SRC_SERVER)/gnutls_server.c
	$(CC) -o $(OBJ_DIR)/$@.o -c $? $(CCFLAGS)

mirror-server: $(SRC_DIR)/$(SRC_SERVER)/mirror-server.c
	$(CC) -o $(BUILD_DIR)/$@ $? $(OBJ_DIR)/common.o $(OBJ_DIR)/base64.o $(OBJ_DIR)/server_common.o $(OBJ_DIR)/gnutls_server.o $(OBJ_DIR)/gnutls_common.o $(CCFLAGS)

client_common: $(SRC_DIR)/$(SRC_CLIENT)/client_common.c
	$(CC) -o $(OBJ_DIR)/$@.o -c $? $(CCFLAGS)

gnutls_client: $(SRC_DIR)/$(SRC_CLIENT)/gnutls_client.c
	$(CC) -o $(OBJ_DIR)/$@.o -c $? $(CCFLAGS)

mirror-client: $(SRC_DIR)/$(SRC_CLIENT)/mirror-client.c
	$(CC) -o $(BUILD_DIR)/$@ $? $(OBJ_DIR)/common.o $(OBJ_DIR)/base64.o $(OBJ_DIR)/client_common.o $(OBJ_DIR)/gnutls_client.o $(OBJ_DIR)/gnutls_common.o $(CCFLAGS)

clean:
	$(RM) $(OBJ_DIR)/*.o
	$(RM) $(BUILD_DIR)/mirror-server
	$(RM) $(BUILD_DIR)/mirror-client
	$(RM) $(SRC_DIR)/common/*~
	$(RM) $(SRC_DIR)/client/*~
	$(RM) $(SRC_DIR)/server/*~
	$(RM) $(SRC_DIR)/*~
	$(RM) $(BUILD_DIR)/*~
	$(RM) *~
