#!/bin/bash

#    mirror-port is a program to mirror your local port to a remote server.
#    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

output_dir="../build/cert"

error_exit() {
  echo "$1" 1>&2
  exit 1
}

echo "deploying certificates..."

mkdir -p /usr/local/share/ca-certificates/mirror-demo-ca/ || error_exit "Cannot create /usr/local/share/ca-certificates/mirror-demo-ca/! Aborting"

if [ ! -f "$output_dir/mirror-ca-cert.crt" ]; then
    echo "could not find $output_dir/mirror-ca-cert.crt"
    exit 1
fi

cp $output_dir/mirror-ca-cert.crt /usr/local/share/ca-certificates/mirror-demo-ca/ || error_exit "Cannot copy mirror-ca-cert.crt certificate to /usr/local/share/ca-certificates/mirror-demo-ca/! Aborting"

/usr/sbin/update-ca-certificates || error_exit "Cannot execute update-ca-certificates! Aborting"

echo

printf "\nTo remove:\n"
printf "\t# ./remove-ca-cert.sh\n"
printf "\tor \n"
printf "\tmanually remove the certificate\n"
printf "\t# rm /usr/local/share/ca-certificates/mirror-demo-ca/mirror-ca-cert.crt\n"
printf "\t# rmdir /usr/local/share/ca-certificates/mirror-demo-ca/\n"
printf "\t# /usr/sbin/update-ca-certificates --fresh\n\n"

