#!/bin/bash

#    mirror-port is a program to mirror your local port to a remote server.
#    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

output_dir="../build/cert"
config_dir="../config"

echo "generating certificates..."

certtool --generate-privkey --outfile $output_dir/key.pem --rsa

certtool --generate-privkey --outfile $output_dir/mirror-ca-key.pem

certtool --generate-self-signed --load-privkey $output_dir/mirror-ca-key.pem --outfile $output_dir/mirror-ca-cert.pem --template $config_dir/server_cert.cfg

certtool --generate-certificate --load-privkey $output_dir/key.pem --outfile $output_dir/cert.pem --load-ca-certificate $output_dir/mirror-ca-cert.pem --load-ca-privkey $output_dir/mirror-ca-key.pem --template $config_dir/client_cert.cfg

certtool --generate-crl --load-ca-privkey $output_dir/mirror-ca-key.pem --load-ca-certificate $output_dir/mirror-ca-cert.pem --outfile $output_dir/crl.pem --template $config_dir/crl_cert.cfg

#certtool --certificate-info --infile $output_dir/mirror-ca-cert.pem --outder --outfile $output_dir/mirror-ca-cert.crt

openssl x509 -inform PEM -in $output_dir/mirror-ca-cert.pem -out $output_dir/mirror-ca-cert.crt

echo "done"
echo

echo "client should run the following command as root:"
printf "\t# ./deploy_ca_cert.sh"
echo
printf "\tor \n"
printf "\tmanually add the certificate\n"
printf "\t# mkdir /usr/local/share/ca-certificates/mirror-demo-ca/\n"
printf "\t# cp $output_dir/mirror-ca-cert.crt /usr/local/share/ca-certificates/mirror-demo-ca/\n"
printf "\t# /usr/sbin/update-ca-certificates\n"

printf "\nTo remove:\n"
printf "\t# rm /usr/local/share/ca-certificates/mirror-demo-ca/mirror-ca-cert.crt\n"
printf "\t# rmdir /usr/local/share/ca-certificates/mirror-demo-ca/\n"
printf "\t# /usr/sbin/update-ca-certificates --fresh\n\n"
