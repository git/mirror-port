#!/bin/bash

#    mirror-port is a program to mirror your local port to a remote server.
#    Copyright (C) 2020  Salahuddin <salahuddin@member.fsf.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

error_exit() {
  echo "$1" 1>&2
  exit 1
}

if [ -f "/usr/local/share/ca-certificates/mirror-demo-ca/mirror-ca-cert.crt" ]; then
    rm /usr/local/share/ca-certificates/mirror-demo-ca/mirror-ca-cert.crt || error_exit "Cannot remove /usr/local/share/ca-certificates/mirror-demo-ca/! Aborting"
fi

/usr/sbin/update-ca-certificates --fresh || error_exit "Could not execute update-ca-certificates --fresh! Aborting"

echo
